import json
import random
from dataclasses import dataclass

from faker import Faker
import requests

API_DOMAIN = 'localhost:8000'
BASE_URL = f'http://{API_DOMAIN}/api'
fake = Faker()


@dataclass
class User:
    """
    User class
    Used for registration, creation of posts, and liking random posts
    """
    email: str
    password: str
    access_token: str = ''


@dataclass
class AutomatedBot:
    """
    Main program Class which does the following:
    1. Creates users
    2. Creates posts by users
    3. Creates likes to random created posts
    """
    number_of_user: int = 0
    max_posts_per_user: int = 0
    max_likes_per_user: int = 0
    users: [User] = ()
    post_ids: [int] = ()

    def __post_init__(self):
        """
        Set up instance variables reading the conf file
        And generating fake user data
        """
        with open('bot_conf.json', 'rb') as f:
            conf = json.load(f)
        self.number_of_user = conf['number_of_users']
        self.max_posts_per_user = conf['max_posts_per_user']
        self.max_likes_per_user = conf['max_likes_per_user']
        self.users = [User(email=fake.email(), password=fake.password()) for _ in range(self.number_of_user)]

    def create_users(self):
        """
        Creating the users by sending requests to "/auth/register"
         and setting the access tokens to users
        """
        for user in self.users:
            response = requests.post(url=BASE_URL+'/auth/register', data={"email": user.email, "password": user.password,
                                                               "confirm_password": user.password})
            user.access_token = response.json()['access']

    def create_posts(self):
        """
        Creating posts by sending requests to /posts
        and saving the post ids for the future requests
        """
        self.post_ids = []
        for user in self.users:
            for _ in range(self.max_posts_per_user):
                response = requests.post(url=BASE_URL+'/posts', data={"title": fake.sentence(nb_words=5),
                                                                           "description": fake.sentence(nb_words=30)},
                                         headers={'Authorization': f"Bearer {user.access_token}"})
                self.post_ids.append(response.json()['id'])

    def like_posts(self):
        """
        Sending the requests by users to like a random created posts
        """
        for user in self.users:
            for _ in range(random.randint(1, len(self.post_ids))):
                post_id = random.choice(self.post_ids)
                requests.post(url=BASE_URL+f'/posts/like/{post_id}',
                              headers={'Authorization': f'Bearer {user.access_token}'})

    def run(self):
        self.create_users()
        self.create_posts()
        self.like_posts()


if __name__ == '__main__':
    bot = AutomatedBot()
    bot.run()
