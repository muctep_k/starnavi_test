from django.contrib.auth.models import AbstractUser, UserManager
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db import models
from django.utils.translation import gettext as _
# Create your models here.
class User(AbstractUser):
    created_at = models.DateTimeField(auto_now_add=True,
                                      null=True,
                                      blank=True)
    updated_at = models.DateTimeField(auto_now=True,
                                      null=True)

    objects = UserManager()

    #

    email = models.EmailField(_('email address'),
                              unique=True,
                              blank=False)
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    username_validator = UnicodeUsernameValidator()
    username = models.CharField(
        _('username'),
        max_length=150,
        unique=False,
        help_text=_(
            'Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )
    last_activity = models.DateTimeField(_('Last activity'), blank=True, null=True)
    USERNAME_FIELD = 'email'

    REQUIRED_FIELDS = []

    #

    def __str__(self):
        return self.email if self.email else f'User object ID:{self.pk}'


class Post(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.CASCADE, related_name='posts')
    title = models.CharField(_("Title"), max_length=64)
    description = models.TextField(_("Description"), max_length=2048)
    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)


class Like(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.SET_NULL, related_name='likes_made', null=True, blank=True)
    post = models.ForeignKey(to=Post, on_delete=models.CASCADE, related_name='likes')
    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)