from django.contrib.auth import get_user_model, get_user
from django.db import transaction
from django.utils import timezone
from django.utils.functional import SimpleLazyObject
from rest_framework_simplejwt.authentication import JWTTokenUserAuthentication
from rest_framework.request import Request
UserModel = get_user_model()


def get_user_jwt(request):
    user = get_user(request)
    if user.is_authenticated:
        return user
    try:
        user_jwt = JWTTokenUserAuthentication().authenticate(Request(request))
        if user_jwt is not None:
            return UserModel.objects.get(id=user_jwt[1]['user_id'])
    except:
        pass
    return user


class UserActivityMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        self.process_request(request)
        return self.get_response(request)

    @transaction.atomic
    def process_request(self, request):
        user = SimpleLazyObject(lambda: get_user_jwt(request))
        if user and not user.is_anonymous:
            user.last_activity = timezone.now()
            user.save()
