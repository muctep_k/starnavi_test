from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView

#
from .views import CustomTokenObtainPairView, CreatePostView, LikeView, AnalyticsView, UserActivityView
from .views import EmailSignUpView

urlpatterns = [
    path('auth/register', EmailSignUpView.as_view(), name='email-register'),
    path('auth/token/obtain', CustomTokenObtainPairView.as_view(),
         name='token_obtain_pair'),
    path('auth/token/refresh', TokenRefreshView.as_view(),
         name='token_refresh'),
    path('posts', CreatePostView.as_view(), name='create_post'),
    path('analytics/', AnalyticsView.as_view(), name='analytics_post'),
    path('posts/like/<int:post_id>', LikeView.as_view(), name='create_like'),
    path('me/activity', UserActivityView.as_view(), name='user_activity')
]
