import json

from django.contrib.auth import get_user_model
from django.db import IntegrityError, transaction
from django.db.models import Count
from django.db.models.functions import TruncDate
from django.utils import timezone
from rest_framework import generics, response, status
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.views import TokenObtainPairView

#
from .models import Post, Like
from .serializers import EmailSignUpSerializer, PostSerializer, UserActivitySerializer
from rest_framework_simplejwt.exceptions import TokenError, InvalidToken
from .serializers import TokenObtainPairSerializer

UserModel = get_user_model()

OK_RESPONSE = {'detail': 'ok'}


class EmailSignUpView(generics.CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = EmailSignUpSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        token = RefreshToken.for_user(user)
        return Response({'access': str(token.access_token)}, status=status.HTTP_201_CREATED)


class CustomTokenObtainPairView(TokenObtainPairView):
    """
    Takes a set of user credentials and returns an access and refresh JSON web
    token pair to prove the authentication of those credentials.
    """
    serializer_class = TokenObtainPairSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        try:
            serializer.is_valid(raise_exception=True)
        except TokenError as e:
            raise InvalidToken(e.args[0])
        serializer.user.last_login = timezone.now()
        serializer.user.save()
        return response.Response(serializer.validated_data,
                                 status=status.HTTP_200_OK)


class UserPostMixin:
    def get_post(self):
        return get_object_or_404(Post, id=self.kwargs.get('post_id', 0))

    def get_user(self):
        return None if self.request.user.is_anonymous else self.request.user


class CreatePostView(generics.CreateAPIView):
    serializer_class = PostSerializer


class AnalyticsView(generics.GenericAPIView):
    permission_classes = [AllowAny]

    def get(self, request, *args, **kwargs):
        likes = self.filter_queryset(Like.objects.all())
        data = self.get_aggregated_data(likes)
        return Response(data, status=status.HTTP_200_OK)

    def filter_queryset(self, queryset):
        date_from, date_to = self.request.GET.get('date_from'), self.request.GET.get('date_to')
        if date_from:
            queryset = queryset.filter(created_at__gte=date_from)
        if date_to:
            queryset = queryset.filter(created_at__lte=date_to)
        return queryset

    def get_aggregated_data(self, queryset):
        queryset = queryset.annotate(day=TruncDate('created_at')).values('day').annotate(count=Count('id'))
        data = [{"day": like['day'].strftime("%d-%m-%Y"), "count": like['count']} for like in queryset]
        return data


class LikeView(generics.CreateAPIView, generics.DestroyAPIView, UserPostMixin):
    permission_classes = [AllowAny]

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        post, user = self.get_post(), self.get_user()
        Like.objects.create(post=post, user=user)
        return Response(OK_RESPONSE, status=status.HTTP_201_CREATED)

    @transaction.atomic
    def destroy(self, request, *args, **kwargs):
        post, user = self.get_post(), self.get_user()
        like = Like.objects.filter(post=post, user=user).first()
        if like:
            like.delete()
        return Response(OK_RESPONSE, status=status.HTTP_204_NO_CONTENT)


class UserActivityView(generics.RetrieveAPIView):
    serializer_class = UserActivitySerializer

    def get_object(self):
        return self.request.user
