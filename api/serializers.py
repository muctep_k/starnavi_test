from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.utils.translation import gettext as _
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.fields import CurrentUserDefault
from rest_framework_simplejwt.serializers import TokenObtainSerializer
from rest_framework_simplejwt.tokens import RefreshToken



#
from api.models import Post

UserModel = get_user_model()

class EmailSignUpSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True,
        error_messages={
            'invalid': _('Mailbox format error.'),
            'required': _(
                'The field is required.'),
            'blank': _(
                'The field is required.'),
        },
        max_length=64)
    password = serializers.CharField(
        required=True,
        error_messages={
            'invalid': _(
                'The entered password is incorrect.'),
        },
        write_only=True,
    )
    confirm_password = serializers.CharField(
        required=True,
        error_messages={
            'invalid': _(
                'The entered password is incorrect.'),
        },
        write_only=True,
    )

    class Meta:
        model = UserModel
        fields = (
            'email',
            'password',
            'confirm_password',
        )

    @staticmethod
    def validate_email(email):
        try:
            user = UserModel.objects.get(email=email)
            if user:
                error_message = 'This email is already in use. ' \
                                'Please enter another email.'
                raise ValidationError(error_message)
        except UserModel.DoesNotExist:
            return email

    def validate(self, attrs):
        password = attrs.get('password')
        confirm_password = attrs.get('confirm_password')
        try:
            assert password == confirm_password
        except AssertionError:
            raise ValidationError(
                {'confirm_password': "Passwords doesn't match"})

        return attrs

    @transaction.atomic
    def save(self, **kwargs):
        password = self.validated_data.pop('password')
        self.validated_data.pop('confirm_password')
        user = UserModel.objects.create(**self.validated_data)
        user.set_password(password)
        user.save()
        return user


class TokenObtainPairSerializer(TokenObtainSerializer):
    @classmethod
    def get_token(cls, user):
        return RefreshToken.for_user(user), user

    def validate(self, attrs):
        data = super().validate(attrs)

        refresh, user = self.get_token(self.user)

        data['refresh'] = str(refresh)
        data['access'] = str(refresh.access_token)

        return data


class PostSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=CurrentUserDefault())
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = Post
        fields = ('id',
                  'title',
                  'description',
                  'user')


class UserActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = UserModel
        fields = ('last_login',
                  'last_activity')


