# SocialNetwork API 

## Setup Django project

The first thing to do is to clone the repository:

```sh
$ git clone git@gitlab.com:muctep_k/starnavi_test.git
```

Create a virtual environment to install dependencies in and activate it:

```sh
$ virtualenv2 --no-site-packages env
$ source env/bin/activate
```

Then install the dependencies:

```sh
(env)$ pip install -r requirements.txt
```
If you want to use your settings, please specify the configurations
in `social_network/settings.py`, default is `db.sqlite3`

Then apply the migrations:

```sh
(env)$ python manage.py migrate
```

Once migrations are applied you can run the server:
```sh
(env)$ cd project
(env)$ python manage.py runserver
```
And navigate to `http://127.0.0.1:8000/`.

## Run automated bot
You can set up your configurations for bot in `bot_conf.json` file.

To run the bot, you can run the `bot.py` script as python file

```sh
(env)$ python bot.py
```
